import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { JsonreaderService } from './jsonreader.service'
import { TableComponent } from './table/table.component';
import { HttpClientModule } from '@angular/common/http';
import * as data from './assets/sample_data.json';

@NgModule({

  declarations: [
    AppComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    // import HttpClientModule after BrowserModule.
    HttpClientModule,

  ],
  providers: [JsonreaderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
