import { TestBed } from '@angular/core/testing';

import { JsonreaderService } from './jsonreader.service';

describe('JsonreaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JsonreaderService = TestBed.get(JsonreaderService);
    expect(service).toBeTruthy();
  });
});
