import { Component } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
// import { Http, Response } from '@angular/common/http';
import { JsonreaderService } from './jsonreader.service'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Capco Challenge';
  mydata: any [] = [];
  constructor(private jsonreader: JsonreaderService) {}
  ngOnInit() {
    this.jsonreader.getJSON().subscribe(data => {
        console.log(data);
          this.mydata = data;
    });
  }

}
