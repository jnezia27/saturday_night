Project Title

This is a table project created in the Angular 4 framework. The technologies used to make the table component Angular4+, Angular CLI, and Karma/Jasmine.

# Ng4Playground

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.s

Prerequisites

Install Angular CLI. This may require uninstalling or upgrading your current version of Node. I strongly encourage installing NVM. This will reduce the amount of terminal errors you'll encounter.

Installing

See instructions for installing Angular CLI below

https://cli.angular.io/

Running Tests

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
